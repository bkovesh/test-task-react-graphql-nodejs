const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const { graphqlExpress, graphiqlExpress } = require('apollo-server-express');
const config = require('./config')
require('dotenv').config();
var app = express();
const isDev = process.env.NODE_ENV.trim()==='production'?false:true



var corsOptions = {
  origin: [config.urlClient],
  credentials: true,
  allowedHeaders: ['*'],
}
app.use(cors())

// The GraphQL endpoint
const schema = require('./graphql/index')
app.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));

// GraphiQL, a visual editor for queries
app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));


const dbOpts = {
  autoIndex: false,
  useNewUrlParser: true,
  useUnifiedTopology: true
}

mongoose.connect(process.env.DB_URL, dbOpts, (err) => {
  if(err) return console.log(err);
  app.listen(isDev ? 5001 : process.env.PORT, () => {
    console.log('App is running!', config.urlClient);
  });
});



module.exports = app;
