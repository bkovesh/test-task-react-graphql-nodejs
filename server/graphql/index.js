const { makeExecutableSchema } = require('graphql-tools');
const User = require('../schemas/UserSchema');
const {GraphQLScalarType} = require('graphql');


const typeDefs = `
  scalar ID
  scalar Email

  type Query {
    user(_id: ID!): User!
    users(skip: Int = 0, limit: Int = 10): [User]
  }
  
  type Mutation {
    createUser(input: CreateUserInput!): User!
    updateUser(_id: ID!, input: UpdateUserInput!): User!
    deleteUser(_id: ID!): User!
  }
  
  type User {
    _id: ID!
    email: Email!
    name: String!
  }
  
  input CreateUserInput {
    email: Email!
    name: String!
  }
  
  input UpdateUserInput {
    email: Email
    name: String
  }
`;


const ID = new GraphQLScalarType({
  name: 'ID',
  description: 'ID type',
  serialize(value) {
    let result = value;
    return result;
  },
  parseValue(value) {
    let result = value;
    return result;
  },
  parseLiteral(ast) {
    return ast.value;
  }
});


const Email = new GraphQLScalarType({
  name: 'Email',
  description: 'Email type',
  serialize(value) {
    let result = value;
    return result;
  },
  parseValue(value) {
    let result = value;
    return result;
  },
  parseLiteral(ast) {
    return ast.value;
  }
});




const resolvers = {
  ID,
  Email,
  Query: {
    user: async (parent, args) => {
      let {_id} = args;
      let result = await User.findById(_id);
      return result;
    },
    users: async (parent, args) => {
      let {skip,limit} = args;
      let result = await User.find({}).skip(skip).limit(limit);
      // console.log(result)
      return result;
    },
  },
  Mutation: {
    createUser: async (parent, args) => {
      console.log('createUser',args)
      let user = new User(args.input);
      let result = await user.save()
      return result;
    },
    updateUser: async (parent, args) => {
      console.log('updateUser',args)
      let {_id} = args;
      let {email, name} = args.input;
      let user = await User.findById(_id);
      user.name = name;
      user.email = email;
      let result = await user.save()
      return result;
    },
    deleteUser: async (parent, args) => {
      let {_id} = args;
      console.log('deleteUser')
      let result = await User.findByIdAndDelete(_id);
      return result;
    },
  },
};



const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});



module.exports = schema;