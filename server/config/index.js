const isDev = process.env.NODE_ENV.trim()==='production'?false:true

const config = {
  urlClient: isDev ? 'http://localhost:3000':'https://test-task-graphql.netlify.app/',
}


module.exports = config;