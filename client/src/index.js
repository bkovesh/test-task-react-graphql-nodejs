import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {ApolloProvider, ApolloClient, InMemoryCache} from "@apollo/client";

const isDev = process.env.NODE_ENV.trim()==='production'?false:true

const config = {
  urlServer: isDev ? 'http://localhost:5001/graphql':'https://test-task-graphql.herokuapp.com/graphql',
}


const client = new ApolloClient({
  uri: config.urlServer,
  cache: new InMemoryCache()
});


ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
