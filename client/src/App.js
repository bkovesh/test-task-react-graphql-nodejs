import React, {useEffect, useState} from 'react';
import { gql, useQuery, useLazyQuery, useMutation } from '@apollo/client';
import { makeStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import Typography from '@material-ui/core/Typography';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import TextField from '@material-ui/core/TextField';




const USERS = gql`
  query ($skip: Int, $limit: Int) {
    users(skip: $skip, limit: $limit) {
      _id
      email
      name
    }
  }
`

const DELETE_USER = gql`
  mutation ($_id: ID!) {
    deleteUser(_id: $_id) {
      _id
      email
      name
    }
  }
`

const CREATE_USER = gql`
  mutation ($input: CreateUserInput!) {
    createUser(input: $input) {
      _id
      email
      name
    }
  }
`

const UPDATE_USER = gql`
  mutation ($_id: ID!, $input: UpdateUserInput!) {
    updateUser(_id: $_id, input: $input) {
      _id
      email
      name
    }
  }
`

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
});


const rows = [
  {_id:'_id',email:'email',name:'name'},
  {_id:'_id',email:'email',name:'name'},
];


function Row(props) {
  const { _id, email, name, } = props.row;
  const [open, setOpen] = useState(false);
  const [edit, setEdit] = useState(false);
  const [formUser, setFormUser] = useState({email, name,});
  const classes = useRowStyles();
  const [deleteUser, { dataDeleteUser }] = useMutation(DELETE_USER);
  const [updateUser, { dataUpdateUser }] = useMutation(UPDATE_USER);

  const handleChange = (e) => {
    setFormUser({...formUser,...{[e.target.name]:e.target.value}})
  }


  return (
  <>
    <TableRow
    className={classes.root}
    >
      <TableCell>
        <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
          {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
        </IconButton>
      </TableCell>
      <TableCell component="th" scope="row">
        {_id}
      </TableCell>
      <TableCell>
        { !edit &&
        <Box
        onClick={() => setEdit(true)}
        >
          {email}
        </Box>
        }
        { edit &&
        <TextField
        label="Email"
        name="email"
        size="small"
        id="standard-basic"
        value={formUser.email}
        onChange={handleChange}
        />
        }
      </TableCell>
      <TableCell>
        {!edit &&
        <Box
        onClick={() => setEdit(true)}
        >
          {name}
        </Box>
        }
        { edit &&
        <TextField
        label="Name"
        name="name"
        size="small"
        id="standard-basic"
        value={formUser.name}
        onChange={handleChange}
        />
        }
      </TableCell>
      <TableCell>
        <Box margin={0}>
          <Button
          color="primary"
          onClick={() => setEdit(!edit)}
          >
            {edit?'No save':'Edit'}
          </Button>
          { edit &&
          <Button
          color="primary"
          onClick={async () => {
            console.log(formUser)
            await updateUser({variables:{
              _id, input:formUser
            }})
          }}
          >
            Save
          </Button>
          }
          <Button
          color="secondary"
          onClick={async () => {
            console.log(_id)
            await deleteUser({variables:{_id}})
          }}
          >
            Delete
          </Button>
        </Box>
      </TableCell>
    </TableRow>
    <TableRow
    style={{
      borderBottom: 'none',
    }}
    >
      <TableCell
      style={{ paddingBottom: 0, paddingTop: 0 }}
      colSpan={6}
      >
        <Collapse in={open} timeout="auto" unmountOnExit>
          <Box margin={3}>
            <Typography variant="h6" gutterBottom component="div">
              {name}
            </Typography>
            <Box>_id:{' '}{_id}</Box>
            <Box>email:{' '}{email}</Box>
          </Box>
        </Collapse>
      </TableCell>
    </TableRow>
  </>
  );
}



function App() {
  const [getUsers, { called, loading, data }] = useLazyQuery(USERS,{
    pollInterval: 500,
    fetchPolicy: 'network-only',
    variables: { skip: 0, limit:10 }
  });
  const [createUser, { dataCreateUser }] = useMutation(CREATE_USER);
  const [formNewUser, setFormNewUser] = useState({
    email: '',
    name: '',
  });


  useEffect(()=>{
    getUsers({variables: { skip: 0, limit:10 }})
  },[])


  const handleChange = (e) => {
    setFormNewUser({...formNewUser,...{[e.target.name]:e.target.value}})
  }



  return (
  <div
  style={{
    display: 'flex',
    flexDirection: 'column',
    alignItems:'center',
    background:'#f4f4f4',
    height:'100vh',
  }}
  >

    <Container
    maxWidth="md"
    style={{
      marginTop:100,
      display: 'flex',
      flexDirection: 'column',
      alignItems:'center',
    }}
    >
      <form noValidate autoComplete="off">
        <Box margin={1}>
          <TextField
          id="standard-basic"
          label="Name"
          name="name"
          onChange={handleChange}
          />
        </Box>
        <Box margin={1}>
          <TextField
          id="standard-basic"
          label="Email"
          name="email"
          onChange={handleChange}
          />
        </Box>
        <Box margin={1}>
          <Button
          color="primary"
          onClick={async () => {
            const {email, name} = formNewUser;
            // console.log(formNewUser)
            let result = await createUser({variables:{input:{email, name}}})
            console.log(result)
          }}
          >
            Add user
          </Button>
        </Box>
      </form>
    </Container>


    <Container
    maxWidth="md"
    style={{
      marginTop:50,
    }}
    >
      <Table
      aria-label="collapsible table"
      style={{
        background:'white',
      }}
      >
        <TableHead>
          <TableRow>
            <TableCell/>
            <TableCell>Id</TableCell>
            <TableCell>Email</TableCell>
            <TableCell>Name</TableCell>
            <TableCell/>
          </TableRow>
        </TableHead>
        <TableBody>
          { loading ?
            <TableRow>
              <TableCell>Loading...</TableCell>
            </TableRow>
          :
          data?.users.map((row,ir) => (
            <Row key={`row-${row.name}-${ir}`} row={row} />
          ))
          }
        </TableBody>
      </Table>
    </Container>

  </div>
  );
}

export default App;
